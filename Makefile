#  _______ _ _ _    __           _
# |__   __(_) | |  / _|         | |
#    | |   _| | |_| |_ __ _  ___| |_ ___  _ __
#    | |  | | | __|  _/ _` |/ __| __/ _ \| '__|
#    | |  | | | |_| || (_| | (__| || (_) | |
#    |_|  |_|_|\__|_| \__,_|\___|\__\___/|_|
# =============================================
# Version: 1.0.0
# Tiltfactor Makefile for javascript projects.
# This file provides linting, compiling from
# es6/es7 down to es5 and compiling react code.
#
# Author: Jesus R. Moreno jesusrmor94@gmail.com

# Setup the different variables for our browser and server environments
BROWSER_SRC = $(wildcard browser/src/*.js)
BROWSER_LIB = $(BROWSER_SRC:browser/src/%.js=browser/public/js/%.js)
SERVER_SRC  = $(wildcard server/src/*.js)
SERVER_LIB 	= $(SERVER_SRC:server/src/%.js=server/lib/%.js)

# By default we want to tell the user the available tasks to run
default:
	@echo
	@echo "You need to specify from one of the tasks below.\n\
	To run a task type: make <task>\n\n\
	init-browser\tSets up a BROWSER project structure.\n\
	init-server\tSets up a SERVER project structure.\n\
	\n\
	lint-browser\tMakes sure that the BROWSER code follows Tiltfactor \
	guidelines.\n\
	lint-server\tMakes sure that the SERVER code follows Tiltfactor \
	guidelines.\n\
	\n\
	build-browser\tLints and builds files in the browser/src folder\n\
	build-server\tLints and builds files in the server/src folder\n\
	"

all: lint-server lint-browser build-server build-browser start-server

start-server:
	@iojs server/lib/index.js
# Sets up the folder structure to use for browser based environments.
init-browser:
	@mkdir -p browser/src
	@touch browser/src/index.js
	@mkdir -p browser/public/js
	@npm init
	@npm install --save babelify

# Sets up the folder structure to use for browser based environments.
init-server:
	@mkdir -p server/src
	@touch server/src/index.js
	@mkdir -p server/lib
	@npm init

# Runs the files in browser/src through our linter.
lint-browser: $(BROWSER_SRC)
	@echo "Linting file for the browser: $<"
	@eslint $<

# Runs the files in server/src through our linter.
lint-server: $(SERVER_SRC)
	@echo "Linting file for the server: $<"
	@eslint $<

# Sets the command that should be run when building the browser files
browser/public/js/%.js: browser/src/%.js
	@echo "Building file for the browser: $<"
	@mkdir -p $(@D)
	@browserify -t [ babelify ] $< -o $@

# Sets the command that should be run when building the server files
server/lib/%.js: server/src/%.js
	@echo "Building file for the server: $<"
	@mkdir -p $(@D)
	@babel $< -o $@

# Specifies the targets
build-browser: lint-browser $(BROWSER_LIB)
build-server: lint-server $(SERVER_LIB)

# Util functions
clean-browser:
	@echo "Removing all built javascript files."
	@rm -f $(BROWSER_LIB)
clean-server:
	@echo "Removing all built javascript files."
	@rm -f $(SERVER_LIB)

nuke:
	@echo
	@echo "WARNING: Will erase the entire directory except for this Makefile \
	and any hidden files."
	@while [ -z "$$CONTINUE" ]; do \
		read -r -p "Type anything but Y or y to exit. [y/N]: " CONTINUE; \
	done ; \
	[ $$CONTINUE = "y" ] || [ $$CONTINUE = "Y" ] || (echo "Exiting."; exit 1;)
	@echo
	@echo "Backing up Makefile"
	@echo
	@cp Makefile .makefile_bak
	@rm -r -v *
	@mv .makefile_bak Makefile
