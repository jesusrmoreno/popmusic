"use strict";

require("babel/register");
var routes = require("./routes.js");
var express = require("express");
var http = require("http");
var path = require("path-extra");
var app = express();
var PORT = process.env.PORT || 8080;

app.set("views", path.join(__dirname, "../../browser/"));
app.engine("html", require("ejs").renderFile);
app.get("/", routes.index);
app.use("/", express["static"](path.join(__dirname, "../../browser/public")));

var server = http.createServer(app).listen(PORT, function () {
  console.log("Server started on port: " + PORT);
});

var io = require("socket.io")(server);
io.on("connection", function (socket) {});

// All socket code goes here
