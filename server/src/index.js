'use strict';
require('babel/register');
let routes  = require('./routes.js');
let express = require('express');
let http    = require('http');
let path    = require('path-extra');
let app     = express();
const PORT  = process.env.PORT || 8080;

app.set('views', path.join(__dirname, '../../browser/'));
app.engine('html', require('ejs').renderFile);
app.get('/', routes.index);
app.use('/', express.static(path.join(__dirname, '../../browser/public')));

const server = http.createServer(app).listen(PORT, () => {
  console.log(`Server started on port: ${PORT}`);
});

let io = require('socket.io')(server);
io.on('connection', (socket) => {
  // All socket code goes here
});
