'use strict';
/*global io socket*/
const React = require('react');

const InputBox = React.createClass({
  getInitialState: function() {
    return {inputValue: ''};
  },
  _onKeyDown: function(e) {
    if (e.key === 'Enter') {
      this.setState({inputValue: ''}, function() {
        this.refs.taskInput.getDOMNode().focus();
      });
    }
  },
  _onChange: function(e) {
    this.setState({
      inputValue: e.target.value
    });
  },
  render: function() {
    return (
      <div className="input-area">
        <input
          ref="taskInput"
          value={this.state.inputValue}
          onChange={this._onChange}
          onKeyDown={this._onKeyDown}
          type="text"
          placeholder="What are you doing?"
          className="task-input"
        >
        </input>
      </div>
    );
  }
});

module.exports = InputBox;
