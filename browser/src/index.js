'use strict';
/* globals Master */
require('babel/register');
let io        = require('socket.io-client');
let socket    = io();
let React     = require('react');
let Immutable = require('immutable');
let InputBox  = require('./components/InputBox.js');

let AppState = new Immutable.Map({
  recentlyPlayed : new Immutable.OrderedMap(),
  nextSong       : '',
  possibleSongs  : new Immutable.OrderedMap(),
  nowPlaying     : '',
  isPlaying      : false,
  searchResults  : new Immutable.List()
});

let Master = React.createClass({
  getInitialState: function() {
    socket.on('new-state', this.handleNewState);
    socket.on('search-results', this.handleResults);
    socket.emit('search');
    return AppState;
  },
  handleNewState: function(data) {
    const newState = AppState.set('recentlyPlayed', data.recentlyPlayed || {})
      .set('possibleSongs', data.possibleSongs || '')
      .set('nowPlaying', data.nowPlaying || '')
      .set('isPlaying', data.isPlaying || false)
      .set('nextSong', data.nextSong || '');
    this.setState(newState);
  },
  handleResults: function(data) {
    const newState = AppState.set('searchResults', data.results);
    this.setState(newState);
  },
  render: function() {
    return (
      <div className="wrapper">
        <InputBox />
      </div>
    );
  }
});

React.render(
  <Master />,
  document.getElementById('root')
);
